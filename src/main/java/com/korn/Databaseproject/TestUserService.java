/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.korn.Databaseproject;

import com.korn.Databaseproject.model.User;
import com.korn.Databaseproject.service.UserService;

/**
 *
 * @author USER
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userservice = new UserService();
        User user = userservice.login("user1", "password");
        if(user!=null){
            System.out.println("Welcome user : " + user.getName());
        }else{
            System.out.println("ERROR");
        }
    }
}
